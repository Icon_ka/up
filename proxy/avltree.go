package main

import (
    "fmt"
    "time"
    "math/rand"
    "log"
    "io/ioutil"
    "strings"
)

type Node struct {
    key    int
    left   *Node
    right  *Node
    height int
}

func max(a, b int) int {
    if a > b {
        return a
    }
    return b
}

func height(N *Node) int {
    if N == nil {
        return 0
    }
    return N.height
}

func newNode(key int) *Node {
    node := &Node{key: key}
    node.left = nil
    node.right = nil
    node.height = 1
    return node
}

func rightRotate(y *Node) *Node {
    x := y.left
    T2 := x.right
    x.right = y
    y.left = T2
    y.height = max(height(y.left), height(y.right)) + 1
    x.height = max(height(x.left), height(x.right)) + 1
    return x
}

func leftRotate(x *Node) *Node {
    y := x.right
    T2 := y.left
    y.left = x
    x.right = T2
    x.height = max(height(x.left), height(x.right)) + 1
    y.height = max(height(y.left), height(y.right)) + 1
    return y
}

func getBalanceFactor(N *Node) int {
    if N == nil {
        return 0
    }
    return height(N.left) - height(N.right)
}

func insertNode(node *Node, key int) *Node {
    if node == nil {
        return newNode(key)
    }
    if key < node.key {
        node.left = insertNode(node.left, key)
    } else if key > node.key {
        node.right = insertNode(node.right, key)
    } else {
        return node
    }

    node.height = 1 + max(height(node.left), height(node.right))
    balanceFactor := getBalanceFactor(node)

    if balanceFactor > 1 {
        if key < node.left.key {
            return rightRotate(node)
        } else if key > node.left.key {
            node.left = leftRotate(node.left)
            return rightRotate(node)
        }
    }

    if balanceFactor < -1 {
        if key > node.right.key {
            return leftRotate(node)
        } else if key < node.right.key {
            node.right = rightRotate(node.right)
            return leftRotate(node)
        }
    }

    return node
}

func nodeWithMinimumValue(node *Node) *Node {
    current := node
    for current.left != nil {
        current = current.left
    }
    return current
}

func printTree(root *Node) string {
    if root == nil {
        return ""
    }

    output := ""

    if root.left != nil {
        output += fmt.Sprintf("%d --> %d\n", root.key, root.left.key)
        output += printTree(root.left)
    }

    if root.right != nil {
        output += fmt.Sprintf("%d --> %d\n", root.key, root.right.key)
        output += printTree(root.right)
    }

    return output
}

func avl() {
    root := insertNode(nil, 33)
    for i := 0; i < 5; i++ {
        root = insertNode(root, rand.Intn(1000))
    }
    t := time.NewTicker(5 * time.Second)
    for {
        select {
        case <-t.C:
            sb := strings.Builder{}
            sb.WriteString("---\nmenu:\n    after:\n        name: binary_tree\n        weight: 2\ntitle: Построение сбалансированного бинарного дерева\n---\n\n\n")
            sb.WriteString("{{< mermaid >}} \n graph TD\n")

            root = insertNode(root, rand.Intn(1000))
            sb.WriteString(printTree(root))

            sb.WriteString("\n")
            sb.WriteString("{{< /mermaid >}}\n")

            err := ioutil.WriteFile("/app/static/tasks/binary.md", []byte(sb.String()), 0644)
            if err != nil {
                log.Fatal("Error write file:", err)
                return
            }
        }
    }
}
