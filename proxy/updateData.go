package main

import (
    "log"
    "time"
    "text/template"
    "os"
)

type Data struct {
    Time    string
    Counter int
}

const tmpl = `
---
menu:
    before:
        name: tasks
        weight: 5
title: Обновление данных в реальном времени
---

# Задача: Обновление данных в реальном времени

Напишите воркер, который будет обновлять данные в реальном времени, на текущей странице.
Текст данной задачи менять нельзя, только время и счетчик.

Файл данной страницы: /app/static/tasks/_index.md

Должен меняться счетчик и время:

Текущее время: {{.Time}}

Счетчик: {{.Counter}}



## Критерии приемки:
- [ ] Воркер должен обновлять данные каждые 5 секунд
- [ ] Счетчик должен увеличиваться на 1 каждые 5 секунд
- [ ] Время должно обновляться каждые 5 секунд
`

func UpdateDataWorker() {
    t := template.Must(template.New("tmpl").Parse(tmpl))

    counter := 0
    ticker := time.NewTicker(5 * time.Second)
    for {
        select {
        case <-ticker.C:
            f, err := os.OpenFile("/app/static/tasks/_index.md", os.O_WRONLY, 0644)
            if err != nil {
                log.Fatal("Error creating file:", err)
                return
            }

            data := Data{
                Time:    time.Now().Format("2006-01-02 15:04:05"),
                Counter: counter,
            }

            err = t.Execute(f, data)
            if err != nil {
                log.Fatal("Error executing template:", err)
                return
            }

            f.Close()
            counter++
        }
    }

}
