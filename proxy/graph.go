package main

import (
    "fmt"
    "math/rand"
    "strconv"
    "time"
    "strings"
    "log"
    "io/ioutil"
)

type graphNode struct {
    ID    int
    Name  string
    Form  string // "circle", "rect", "square", "ellipse", "round-rect", "rhombus"
    Links []*graphNode
}

func Mermaid() {
    t := time.NewTicker(5 * time.Second)
    for {
        select {
        case <-t.C:
            sb := strings.Builder{}
            sb.WriteString("---\nmenu:\n    after:\n        name: graph\n        weight: 1\ntitle: Построение графа\n---\n\n\n")
            sb.WriteString("{{< mermaid >}} \n graph LR\n")

            graph := buildGraph()
            for _, node := range graph {
                for _, link := range node.Links {
                    sb.WriteString(fmt.Sprintf("%s%s --> %s%s\n", "node"+strconv.Itoa(node.ID), node.Form, "node"+strconv.Itoa(link.ID), link.Form))
                }
            }

            sb.WriteString("\n")
            sb.WriteString("{{< /mermaid >}}\n")

            err := ioutil.WriteFile("/app/static/tasks/graph.md", []byte(sb.String()), 0644)
            if err != nil {
                log.Fatal("Error write file:", err)
                return
            }
        }
    }
}

func buildGraph() []*graphNode {
    graph := []*graphNode{}
    rand.Seed(time.Now().UnixNano())

    numNodes := rand.Intn(26) + 5

    for i := 1; i <= numNodes; i++ {
        node := &graphNode{
            ID:    i,
            Name:  fmt.Sprintf("Node %d", i),
            Form:  getRandomForm(),
            Links: []*graphNode{},
        }
        graph = append(graph, node)

        if i > 1 {
            randomIndex := rand.Intn(i - 1)
            randomNode := graph[randomIndex]
            node.Links = append(node.Links, randomNode)
            randomNode.Links = append(randomNode.Links, node)
        }
    }

    return graph
}

func getRandomForm() string {
    forms := []string{"((Circle))", "[Rect]", "[Square]", `(Ellipse)`, "(Round Rect)", "{Rhombus}"}
    return forms[rand.Intn(len(forms))]
}
