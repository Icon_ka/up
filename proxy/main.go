package main

import (
    "fmt"
    "github.com/go-chi/chi"
    "net/http"
    "net/http/httputil"
    "net/url"
)

func main() {
    go Mermaid()
    go UpdateDataWorker()
    go avl()
    r := chi.NewRouter()

    proxy := NewReverseProxy("hugo", "1313")
    r.Use(proxy.ReverseProxy)
    r.Get("/api/", apiHelloHandler)
    http.ListenAndServe(":8080", r)
}

func apiHelloHandler(w http.ResponseWriter, r *http.Request) {
    w.Write([]byte("Hello from API"))
}

type ReverseProxy struct {
    host string
    port string
}

func NewReverseProxy(host, port string) *ReverseProxy {
    return &ReverseProxy{
        host: host,
        port: port,
    }
}

func (rp *ReverseProxy) ReverseProxy(next http.Handler) http.Handler {
    target, _ := url.Parse(fmt.Sprintf("http://%s:%s", rp.host, rp.port))
    proxy := httputil.NewSingleHostReverseProxy(target)
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        if r.URL.Path == "/api/" {
            next.ServeHTTP(w, r)
        } else {
            proxy.ServeHTTP(w, r)
        }
    })
}
